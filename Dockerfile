FROM python:3.5-buster

ADD app.py /

ENV FLASK_APP=app.py

COPY requirements.txt /tmp/

RUN pip install --requirement /tmp/requirements.txt

CMD flask run --port 7777
